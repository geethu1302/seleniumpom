package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {

	@FindBy(how=How.PARTIAL_LINK_TEXT,using="Create Lead") WebElement eleCreateLead;
	public MyLeadsPage() {
		PageFactory.initElements(driver,this);
	}
	
	public CreateLeadPage clickCreateLeads() {
		
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	
	
}
