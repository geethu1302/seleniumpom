package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	
	public ViewLeadPage() {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how=How.XPATH,using="//span[@id='viewLead_firstName_sp']") WebElement eleFirstNameText;
	public ViewLeadPage verifyName(String data) {
		verifyExactText(eleFirstNameText, data);
		return this;
		
		
		
	}

}
